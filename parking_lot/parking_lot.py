parking_lot = input("Create parking lot = ")

if not parking_lot:
  print("Please make parking lot")
else:
  parking_lot = int(parking_lot)

# CREATE PARKING DATABASE
if parking_lot:
  slot = 1 
  car_list = []
  for lot in range(parking_lot):
    car_data = {}
    car_data["slot"] = slot
    car_data["colour"] = ''
    car_data["registration_no"] = ''
    car_data['vacant'] = True
    car_list.append(car_data)
    slot += 1

def park_a_car(command):
  car = command.split()

  # CHECK IF CAR DATA IS VALID
  if not car:
    print("You must input car data")

  # CHECK IF CAR DATA IS VALID
  if len(car) != 3:
    print("Wrong parking format")
  
  if car:
    car_registration_no = car[1]
    car_colour = car[2]
    if all(database['vacant'] == False for database in car_list):
      print("Sorry, parking lot is full")
    else:
      for database in car_list:
        if database['vacant'] == True:
          database['colour'] = car_colour
          database['registration_no'] = car_registration_no
          database['vacant'] = False
          print("Allocated slot number ",database['slot'])
          break
  
  input_command()

def get_status():
  data_header = ['Slot No.', 'Registration No.', 'Colour']
  header = ''
  for title in data_header:
    header += title + '  '
  
  if car_list:
    for database in car_list:
      header += ('\n' + str(database['slot']) + (' ' * (10 - len(str(database['slot'])))) + database['registration_no'] + (' ' * (18 - len(database['registration_no']))) + database['colour'])
  
  print(header)
  input_command()
  
def reset_slot(command):
  reset_slot_data = command.split()

  if not reset_slot_data:
    print("You must input slot data")

  if len(reset_slot_data) != 2:
    print("Wrong parking format")
  
  if reset_slot_data:
    leaving_slot = reset_slot_data[1]
    lot_checked = False
    for database in car_list:
      if database['slot'] == int(leaving_slot):
        database['registration_no'] = ''
        database['colour'] = ''
        database['vacant'] = True
        lot_checked = True
        print("Slot number",leaving_slot,"is free")
        break
    
    if not lot_checked:
      print("Not Found")

  input_command()

def check_registration_number_with_colour(command):
  check_regs_number = command.split()

  if not check_regs_number:
    print("You must input corect command")

  if len(check_regs_number) != 2:
    print("Wrong parking format")

  if check_regs_number:
    colour_check = check_regs_number[1]
    list_of_regs_number = ''
    regs_number_check = False
    for database in car_list:
      if database['colour'].lower() == colour_check.lower():
        list_of_regs_number += database['registration_no'] + ', '
        regs_number_check = True
    
    print(list_of_regs_number)

    if not regs_number_check:
      print("Not Found")
  
  input_command()

def check_slot_number_with_colour(command):
  check_slot_number = command.split()

  if not check_slot_number:
    print("You must input correct command")

  if len(check_slot_number) != 2:
    print("Wrong parking format")

  if check_slot_number:
    colour_check = check_slot_number[1]
    list_of_slot = ''
    slot_check = False
    for database in car_list:
      if database['colour'].lower() == colour_check.lower():
        list_of_slot += str(database['slot']) + ', '
        slot_check = True
    
    print(list_of_slot)

    if not slot_check:
      print("Not Found")
  
  input_command()

def check_slot_number_with_regs_no(command):
  check_slot_number = command.split()

  if not check_slot_number:
    print("You must input correct command")

  if len(check_slot_number) != 2:
    print("Wrong parking format")

  if check_slot_number:
    regs_no = check_slot_number[1]
    list_of_slot = ''
    slot_check = False
    for database in car_list:
      if database['registration_no'].lower() == regs_no.lower():
        list_of_slot += str(database['slot']) + ', '
        slot_check = True
    
    print(list_of_slot)

    if not slot_check:
      print("Not Found")
  
  input_command()

def input_command():
  print("\n\n***Please input command***\n\n")
  command = input()
  if not command:
    print("Please input command")
    input_command()
    
  order = command.split()[0]
  if order.lower() == "status":
    get_status()
  elif order.lower() == "park":
    park_a_car(command)
  elif order.lower() == "leave":
    reset_slot(command)  
  elif order.lower() == "registration_numbers_for_cars_with_colour":
    check_registration_number_with_colour(command)
  elif order.lower() == "slot_numbers_for_cars_with_colour":
    check_slot_number_with_colour(command)
  elif order.lower() == "slot_number_for_registration_number":
    check_slot_number_with_regs_no(command)
  elif order.lower() == "exit":
    exit()
  else:
    print("Incorrect command")
    input_command()

input_command()

